/**
 * @file cpp_vertex_to_element.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include "PatranLoader.h"

#include "tetra_mesh_utils.h"

#include "simple_brick_vertex_to_element.h"

using namespace std;
using namespace boost;

void test_vertex_to_element(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: vertex_to_element_mapping() function                              //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_vertex_to_element(status);
  if (status == false) {
    cout << "FAILED test_vertex_to_element()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_vertex_to_element()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_vertex_to_element(bool &test_status)
{
  test_status = true;
  string fileName("mesh/cube/005/simple_brick.pat");

  simple_brick_vertex_to_element expected;

  PatranLoader loader;
  
  multi_array<double, 2> vcoord;
  multi_array<int,    2> eindex;
  multi_array<int,    2> mapping;

  loader.load(fileName, ONE_INDEXING, vcoord, eindex);

  vertex_to_element_mapping(vcoord, eindex, mapping);

  // Check that the number of rows/columns is correct.
  if (mapping.shape()[0] != expected.n) {
    cout << "Expected number of rows " << expected.n << ", but got " << 
      mapping.shape()[0] << endl;
    test_status = false;
    return;
  }
  if (mapping.shape()[1] != expected.m) {
    cout << "Expected number of rows " << expected.m << ", but got " << 
      mapping.shape()[1] << endl;
    test_status = false;
    return;
  }

  // Check individual values for rows/columns
  for (int i = 0; i < mapping.shape()[0]; ++i) {
    for (int j = 0; j < mapping.shape()[1]; ++j) {
      if (mapping[i][j] != expected.mapping[i][j]) {
        cout << "Expected mapping value " << expected.mapping[i][j] << 
          ", but got " << mapping[i][j] << " for indices i=" << i << ", j=" << j << endl;
        test_status = false;
        return;
      }
    }
  }

  // Check individual values for rows/columns
  for (int i = 0; i < mapping.shape()[0]; ++i) {
    cout << setw(3) << i << " ";
    for (int j = 0; j < mapping.shape()[1]; ++j) {
      cout << setw(3) << mapping[i][j] << " ";
    }
    cout << endl;
  }
}
