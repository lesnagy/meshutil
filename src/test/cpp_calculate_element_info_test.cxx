/**
 * @file   cpp_calculate_element_info_test.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <string>
#include <vector>

#include <tetra_mesh_utils.h>

using namespace std;
using namespace boost;

void test_calculate_element_info(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: calculate_element_info() function                                 //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_calculate_element_info(status);
  if (status == false) {
    cout << "FAILED test_calculate_element_info()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_calculate_element_info()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_calculate_element_info(bool &test_status)
{
  test_status = false;
}
