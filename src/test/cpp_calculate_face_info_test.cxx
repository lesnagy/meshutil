/**
 * @file   cpp_calculate_face_info_test.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <string>
#include <vector>

#include <tetra_mesh_utils.h>

using namespace std;
using namespace boost;

void test_calculate_face_info(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: calculate_face_info() function                                    //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_calculate_face_info(status);
  if (status == false) {
    cout << "FAILED test_calculate_face_info()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_calculate_face_info()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_calculate_face_info(bool &test_status)
{
  test_status = true;
  double epsilon = 1E-6;

  multi_array<double, 2> vertices(extents[4][3]);
  vertices[0][0] =  1.0; vertices[0][1] =  0.0; vertices[0][2] = -1.0;
  vertices[1][0] = -1.0; vertices[1][1] =  0.0; vertices[1][2] = -1.0;
  vertices[2][0] =  0.0; vertices[2][1] =  1.0; vertices[2][2] =  1.0;
  vertices[3][0] =  0.0; vertices[3][1] = -1.0; vertices[3][2] =  1.0;

  multi_array<int, 2> elements(extents[1][4]);
  elements[0][0] = 0; elements[0][1] = 1; 
  elements[0][2] = 2; elements[0][3] = 3;

  // Expected values for the four faces.
  int ewinding[4] = {RIGHT_HANDED_WINDING,
                     LEFT_HANDED_WINDING,
                     RIGHT_HANDED_WINDING,
                     LEFT_HANDED_WINDING};

  double ef[4][3] = {
    { 0.0,        0.333333, -0.333333},
    { 0.0,       -0.333333, -0.333333},
    { 0.333333,   0.0,       0.333333},
    {-0.333333,   0.0,       0.333333}
  };

  double en[4][3] = {
    { 0.0,       0.894427, -0.447214},
    { 0.0,      -0.894427, -0.447214},
    { 0.894427,  0.0,       0.447214},
    {-0.894427,  0.0,       0.447214}
  };

  int fidx[4][3] = { {0, 1, 2},   
                     {0, 1, 3},            
                     {0, 2, 3},   
                     {1, 2, 3} };

  int awinding;
  double afx, afy, afz;
  double anx, any, anz;

  /////////////////////////////////////////////////////////////////////////////
  // Test face information is correct for the four faces.                    //
  /////////////////////////////////////////////////////////////////////////////
  for (int i = 0; i < 4; ++i) {

    calculate_face_info(vertices,
                        0, 1, 2, 3,
                        fidx[i][0], fidx[i][1], fidx[i][2],
                        awinding,
                        afx, afy, afz,
                        anx, any, anz);

    if (awinding != ewinding[i]) {
      cout << "Winding incorrect, expected "
           << (ewinding[i] == LEFT_HANDED_WINDING ? "left" : "right")
           << " winding, but got "
           << (awinding == LEFT_HANDED_WINDING ? "left" : "right") 
           << " winding, for face " << i << endl;
      test_status = false;
    }

    if (fabs(ef[i][0] - afx) > epsilon) {
      cout << "Expected " << ef[i][0] << " for x component of face " << i << " centre, "
        "but got " << afx << endl;
      test_status = false;
    }
    if (fabs(ef[i][1] - afy) > epsilon) {
      cout << "Expected " << ef[i][1] << " for y component of face " << i << " centre, "
        "but got " << afy << endl;
      test_status = false;
    }
    if (fabs(ef[i][2] - afz) > epsilon) {
      cout << "Expected " << ef[i][2] << " for z component of face " << i << " centre, "
        "but got " << afz << endl;
      test_status = false;
    }

    if (fabs(en[i][0] - anx) > epsilon) {
      cout << "Expected " << en[i][0] << " for x component of face " << i << " normal, "
        "but got " << anx << endl;
      test_status = false;
    }

    if (fabs(en[i][1] - any) > epsilon) {
      cout << "Expected " << en[i][1] << " for y component of face " << i << " normal, "
        "but got " << any << endl;
      test_status = false;
    }

    if (fabs(en[i][2] - anz) > epsilon) {
      cout << "Expected " << en[i][2] << " for z component of face " << i << " normal, "
        "but got " << anz << endl;
      test_status = false;
    }
  }

}
