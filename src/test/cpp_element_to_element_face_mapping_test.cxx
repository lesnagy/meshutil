/**
 * @file   cpp_element_to_element_face_mapping_test.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <string>
#include <vector>

#include "tetra_mesh_utils.h"

using namespace std;
using namespace boost;

void test_element_to_element_face_mapping(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: element_to_element_face_mapping() function                                          //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_element_to_element_face_mapping(status);
  if (status == false) {
    cout << "FAILED test_element_to_element_face_mapping()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_element_to_element_face_mapping()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_element_to_element_face_mapping(bool &test_status)
{
  test_status = false;
}
