/**
 * @file   cpp_extract_faces_test.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include "PatranLoader.h"

#include "tetra_mesh_utils.h"

using namespace std;
using namespace boost;

void test_extract_faces(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: extract_faces() function                                          //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_extract_faces(status);
  if (status == false) {
    cout << "FAILED test_extract_faces()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_extract_faces()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_extract_faces(bool &test_status) 
{
  test_status = false;
  string fileName("mesh/cube/005/simple_brick.pat");

  PatranLoader loader;

  multi_array<double, 2> vcoord;
  multi_array<int,    2> eindex;
  multi_array<int,    2> findex;

  loader.load(fileName, ONE_INDEXING, vcoord, eindex);

  extract_faces(vcoord, eindex, findex);

  for (int i = 0; i < findex.shape()[0]; ++i) {
    cout << setw(3) << i << " ";
    for (int j = 0; j < findex.shape()[1]; ++j) {
      cout << setw(3) << findex[i][j] << " ";
    }
    cout << endl;
  }

}
