/**
 * @file   cpp_padded_number.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <string>
#include <vector>

#include "tetra_mesh_utils.h"

using namespace std;
using namespace boost;

void test_padded_number(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: padded_number() function                                          //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_padded_number(status);
  if (status == false) {
    cout << "FAILED test_padded_number()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_padded_number()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_padded_number(bool &test_status)
{
  test_status = true;

  string padded_number_1 = padded_number(0,  1);
  string padded_number_2 = padded_number(0,  2);
  string padded_number_3 = padded_number(10, 1);
  string padded_number_4 = padded_number(10, 2);
  string padded_number_5 = padded_number(10, 3);

  if (padded_number_1.compare("0") != 0) {
    test_status = false;
    return;
  }

  if (padded_number_2.compare("x0") != 0) {
    test_status = false;
    return;
  }

  if (padded_number_3.compare("10") != 0) {
    test_status = false;
    return;
  }

  if (padded_number_4.compare("10") != 0) {
    test_status = false;
    return;
  }

  if (padded_number_5.compare("x10") != 0) {
    test_status = false;
    return;
  }
}
