/**
 * @file   cpp_has_indices_test.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <string>
#include <vector>

#include "tetra_mesh_utils.h"

using namespace std;
using namespace boost;

void test_hash_indices(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: hash_indices() function.                                          //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_hash_indices(status);
  if (status == false) {
    cout << "FAILED test_hash_indices()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_hash_indices()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_hash_indices(bool &test_status) 
{
  test_status = true;

  string hash_indices_1 = hash_indices({0,1,2,3}, 10);
  string hash_indices_2 = hash_indices({20,21,22,33}, 10);
  string hash_indices_3 = hash_indices({33,32,21,20}, 10);
  string hash_indices_4 = hash_indices({2000010101,21,2009092,0}, 10);
  string hash_indices_5 = hash_indices({32,0,4}, 5);

  if (hash_indices_1.compare("xxxxxxxxx0xxxxxxxxx1xxxxxxxxx2xxxxxxxxx3") != 0) {
    test_status = false;
    return;
  }

  if (hash_indices_2.compare("xxxxxxxx20xxxxxxxx21xxxxxxxx22xxxxxxxx33") != 0) {
    test_status = false;
    return;
  }

  if (hash_indices_3.compare("xxxxxxxx20xxxxxxxx21xxxxxxxx32xxxxxxxx33") != 0) {
    test_status = false;
    return;
  }

  if (hash_indices_4.compare("xxxxxxxxx0xxxxxxxx21xxx20090922000010101") != 0) {
    test_status = false;
    return;
  }

  if (hash_indices_5.compare("xxxx0xxxx4xxx32") != 0) {
    test_status = false;
    return;
  }
}
