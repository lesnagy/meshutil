/**
 * @file   cpp_surface_vertex_to_surface_face_mapping_test.cxx
 * @author L. Nagy
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include <boost/format.hpp>

#include "PatranLoader.h"

#include "tetra_mesh_utils.h"

using namespace std;
using namespace boost;

void test_surface_vertex_to_surface_face_mapping(bool &test_status);

int main(int argc, char **argv)
{
  bool all_status = true;
  bool status = true;

  /////////////////////////////////////////////////////////////////////////////
  // TEST: surface_vertex_to_surface_face_mapping() function                 //
  /////////////////////////////////////////////////////////////////////////////
  status = true;
  test_surface_vertex_to_surface_face_mapping(status);
  if (status == false) {
    cout << "FAILED test_surface_vertex_to_surface_face_mapping()" << endl;
    all_status = false;
  } else {
    cout << "OK     test_surface_vertex_to_surface_face_mapping()" << endl;
  }

  if (all_status == false) {
    return 1;
  }
  return 0;
}

void test_surface_vertex_to_surface_face_mapping(bool &test_status) 
{
  test_status = false;
    test_status = false;
  string fileName("mesh/cube/005/simple_brick.pat");

  PatranLoader loader;
  
  multi_array<double, 2> vcoord;
  multi_array<int,    2> eindex;
  multi_array<int,    2> findex;
  multi_array<int,    2> fe_mapping;
  multi_array<int,    2> mapping;

  loader.load(fileName, ONE_INDEXING, vcoord, eindex);

  extract_faces(vcoord, eindex, findex);
  face_to_element_mapping(vcoord, eindex, findex, fe_mapping);
  surface_vertex_to_surface_face_mapping(vcoord, findex, fe_mapping, mapping);

  // Check individual values for rows/columns
  for (int i = 0; i < mapping.shape()[0]; ++i) {
    cout << setw(3) << i << " ";
    for (int j = 0; j < mapping.shape()[1]; ++j) {
      cout << setw(3) << mapping[i][j] << " ";
    }
    cout << " --- ";
    cout << format("%10.5f, %10.5f, %10.5f") % vcoord[i][0] % vcoord[i][1] % vcoord[i][2]; 
    cout << endl;
  }

}
