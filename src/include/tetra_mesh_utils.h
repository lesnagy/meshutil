/* 
 * @file   tetra_mesh_utils.h
 * @author L. Nagy
 */

#ifndef TETRA_MESH_UTILS_H
#define TETRA_MESH_UTILS_H

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include <boost/multi_array.hpp>

#define LEFT_HANDED_WINDING  1
#define RIGHT_HANDED_WINDING 2

/**
 * Function to enumerate the element indices associated with each vertex index.
 * @vertices an NVERT by 3 array of vertex coordinates (where NVERT is the
 *           number of mesh vertices).
 * @elements an NELEM by 4 array of element indices (where NELEM is the number
 *           of mesh elements).
 * @mapping  the output mapping with each row (there are NVERT rows) 
 *           representing the elements associated with the respective vertex, 
 *           i.e. row index a value of -1 represents no element for the mapping.
 */
void vertex_to_element_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &elements,
    boost::multi_array<int,    2>       &mapping);

/**
 * Function to compute a vertex to face mapping.
 */
void vertex_to_face_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &faces,
    boost::multi_array<int,    2>       &mapping);

/**
 * Function to extract all faces from the mesh.
 */
void extract_faces(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &elements,
    boost::multi_array<int,    2>       &faces);

/**
 * Function to construct a mapping from faces to elements.
 */
void face_to_element_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &elements,
    boost::multi_array<int,    2> const &faces,
    boost::multi_array<int,    2>       &mapping);

/**
 * Function to extract surface faces from a mesh.
 */
void extract_surface_faces(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &elements,
    boost::multi_array<int,    2> const &f2e,
    boost::multi_array<int,    2>       &surfaceFaces);

/**
 * Function to construct a mapping betwen elements based on face connectivity.
 */
void element_to_element_face_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<double, 2> const &elements,
    boost::multi_array<double, 2>       &mappping);

/**
 * Function to construct a mapping between surface vertices and faces.
 */
void surface_vertex_to_surface_face_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &faces,
    boost::multi_array<int,    2> const &fe_mapping,
    boost::multi_array<int,    2>       &mapping);

/**
 * Function to produce a hash from number in the `indices` array - each such 
 * number needs to be padded so that it is of size `maxlen`.
 */
std::string hash_indices(std::vector<int> indices, size_t maxlen);

/**
 * Produce a string version of `number` padded with a character so that it is
 * of length `maxlen`.
 */
std::string padded_number(int number, size_t maxlen);

/**
 * Calculate some useful values for a face.
 */
void calculate_face_info(boost::multi_array<double, 2> const &vertices,
                         int n0, int n1, int n2, int n3,
                         int f0, int f1, int f2,
                         int &w,
                         double &fx, double &fy, double &fz,
                         double &nx, double &ny, double &nz);

/**
 * Calculate some useful values for an element.
 */
void calculate_element_info(boost::multi_array<double, 2> const &vertices,
                            int n0, int n1, int n2, int n3,
                            double &cx, double &cy, double &cz,
                            double &vol, double volsign);

#endif
