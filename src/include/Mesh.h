/**
 * @file   Mesh.h
 * @author L. Nagy
 */

#ifndef MESH_H
#define MESH_H

#include <vector>
#include <algorithm>
#include <string>
#include <unordered_map>

#include <boost/multi_array.hpp>
#include <boost/ptr_container/ptr_vector.hpp>

#include "Vertex.h"
#include "Element.h"

class Mesh
{
  public:

    Mesh(boost::multi_array<double, 2> const &vertices,
         boost::multi_array<int,    2> const &elements);

    /**
     * Return an iterator to the vertices of the mesh.
     */
    // iterator<Vertex> vertices();

    /**
     * Return an iterator to the elements of the mesh.
     */
    //iterator<Element> elements();

    /**
     * Return an iterator to the faces of the mesh.
     */
    //iterator<Face> faces();

    /**
     * Return an iterator to the edges of the mesh.
     */
    //iterator<Edge> edges();

    /**
     * Return an iterator to the boundary vertices of the mesh.
     */
    //iterator<Vertex> boundaryVertices();

    /** 
     * Return an iterator to the boundary elements of the mesh.
     */
    //iterator<Element> boundaryElements();

    /**
     * Return an iterator to the boundary faces of the mesh.
     */
    //iterator<Face> boundaryFaces();

    /**
     * Return an iterator to the boundary edges of the mesh.
     */
    //iterator<Edge> boundaryEdges();

  private:
    std::vector<Vertex>  _vertices;
    std::vector<Element> _elements;

    //std::vector<Face>    faces;
    //std::vector<Edge>    edges;

    /**
     * Extract all unique faces.
     */
    void extractFaces(boost::multi_array<double, 2> const &vertices,
                      boost::multi_array<int,    2> const &elements,
                      boost::multi_array<int,    2>       &faces);

    /**
     * Extract all edges.
     */
    void extractEdges(boost::multi_array<double, 2> const &vertices,
                      boost::multi_array<int,    2> const &elements,
                      boost::multi_array<int,    2>       &edges);

    /**
     * Map elements to elements by face.
     */
    void elemToElem(boost::multi_array<double, 2>    const &vertices,
                    boost::multi_array<int,    2>    const &elements,
                    std::vector< std::vector<int> >        &vtrxConnect,
                    std::vector< std::vector<int> >        &edgeConnect,
                    std::vector< std::vector<int> >        &faceConnect);

    /**
     * Calculate a hash for a given for the list of indices, `maxLen` is used
     * for padding purposes.
     */
    std::string hashIndices(std::vector<int> &indices, 
                            int               maxLen);
};

#endif
