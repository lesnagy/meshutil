/**
 * @file   Face.h
 * @author L. Nagy
 */

#ifndef FACE_H
#define FACE_H

class Face
{
  public:
    /**
     * Create a face.
     */
    Face();
    
    /**
     * Iterator to the vertices belonging to this face.
     */
    iterator<Vertices> vertices();

    /**
     * Iterator to the edges belonging to this face.
     */
    iterator<Edge> edges();

    /**
     * Iterator to edge neighbours.
     */
    iterator<Face> edgeNeighbours();

    /**
     * Iterator to vertex neighbours.
     */
    iterator<Face> vertexNeighbours();

    /**
     * Iterator to the sub-meshes that this face belongs to.
     */
    iterator<int> subMeshes();

    /**
     * Query whether this is a boundary face.
     */
    bool isBoundary();
};

#endif
