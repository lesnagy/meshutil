/**
 * @file   Element.h
 * @author L. Nagy
 */

#ifndef ELEMENT_H
#define ELEMENT_H

#include "Mesh.h"

class Element
{
  friend class Mesh;
  public:
    Element() {}

    /**
     * Query whether this is a boundary element.
     */
    bool isBoundary() { return _isBoundary; }

  private:

    // Vertices comprising this element.
    Vertex *_v0, *_v1, *_v2, *_v3; 

    // Neighbours of this element.
    boost::ptr_vector<Element> _vtrxNeighbours;
    boost::ptr_vector<Element> _edgeNeighbours;
    boost::ptr_vector<Element> _faceNeighbours;

    // Is this a boundary element?
    bool _isBoundary;
};

#endif
