/**
 * @file   Edge.h
 * @author L. Nagy
 */

#ifndef EDGE_H
#define EDGE_H

class Edge
{
  public:
    /**
     * Create an edge.
     */
    Edge();

    /**
     * Iterator to vertices belonging to this edge.
     */
    iterator<Vertex> vertices();

    /**
     * Iterator to vertex neighbours.
     */
    iterator<Edge> vertexNeighbours();

    /**
     * Iterator to list of sub-meshes that this edge belongs to.
     */
    iterator<int> subMeshes;

    /**
     * Query whether this is a boundary edge.
     */
    bool isBoundary();

};

#endif
