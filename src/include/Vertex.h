/**
 * @file   Vertex.h
 * @author L. Nagy
 */

#ifndef VERTEX_H
#define VERTEX_H

#include "Mesh.h"

class Vertex
{
  friend class Mesh;
  public:

    Vertex() {}

    /**
     * Query whether this vertex is a boundary vertex.
     */
    bool isBoundary() { return _isBoundary; }

  private:


    double _cx, _cy, _cz;

    bool _isBoundary;

};

#endif
