/**
 * @file   mesh.cpp
 * @author L. Nagy
 */

#include "Mesh.h"

using namespace std;
using namespace boost;

Mesh::Mesh(multi_array<double, 2> const &vertices,
           multi_array<int,    2> const &elements)
{
  //multi_array<int, 2> faces;
  //multi_array<int, 2> edges;
  //extractFaces(vertices, elements, faces);
  //extractEdges(vertices, elements, edges);
  
  /*
  elementToElementMapByVtrx(vertices,
                            element,
                            elementToElementMapByVtrx);
  elementToElementMapByEdge(vertices,
                            elements,
                            edges,
                            elementToElementMapByEdge);
  */
  /*
  elementToElementMapByFace(vertices,
                            elements,
                            faces,
                            elementToElementMapByFace);
  */

  vector< vector<int> > e2eVtrxConnect;
  vector< vector<int> > e2eEdgeConnect;
  vector< vector<int> > e2eFaceConnect;
  elemToElem(vertices, elements, e2eVtrxConnect, 
                                 e2eEdgeConnect,
                                 e2eFaceConnect);

  // Construct lists of empty vertices & elements.
  _vertices.resize(vertices.size());
  _elements.resize(elements.size());

  // Update vertex information.
  for (int i = 0; i < vertices.size(); ++i) {
    _vertices[i]._cx = vertices[i][0];
    _vertices[i]._cy = vertices[i][1];
    _vertices[i]._cz = vertices[i][2];
  }

  // Update element information.
  for (int i = 0; i < elements.size(); ++i) {
    Element &e = _elements[i];
    
    // First update vertex references.
    e._v0 = &_vertices[elements[i][0]];
    e._v1 = &_vertices[elements[i][1]];
    e._v2 = &_vertices[elements[i][2]];
    e._v3 = &_vertices[elements[i][3]];

    // Update vertex neighbours.
    vector<int> &vtrxNeighbours = e2eVtrxConnect[i];
    for (int j = 0; j < vtrxNeighbours.size(); ++j) {
      e._vtrxNeighbours.push_back(&_elements[vtrxNeighbours[j]]);
    }

    // Update edge neighbours.
    vector<int> &edgeNeighbours = e2eEdgeConnect[i];
    for (int j = 0; j < edgeNeighbours.size(); ++j) {
      e._edgeNeighbours.push_back(&_elements[edgeNeighbours[j]]);
    }

    // Update face neighbours.
    vector<int> &faceNeighbours = e2eFaceConnect[i];
    for (int j = 0; j < faceNeighbours.size(); ++j) {
      e._faceNeighbours.push_back(&_elements[faceNeighbours[j]]);
    }
  }

}

void Mesh::extractFaces(multi_array<double, 2> const &vertices,
                        multi_array<int,    2> const &elements,
                        multi_array<int,    2>       &faces)
{
  int n0, n1, n2, n3;
  vector<int> faceIdx = {0, 1, 2,   0, 1, 3,   0, 2, 3,  1, 2, 3};
  unordered_map<string, int> faceLookup;
  vector< tuple<int , int, int> > faceIndices;

  int maxLen = vertices.size();

  for (int eidx = 0; eidx < elements.size(); ++eidx) {
    int fIdx = 0;
    for (int i = 0; i < 4; ++i) {
      // Face nodes.
      int fn0 = elements[eidx][faceIdx[3*i + 0]];
      int fn1 = elements[eidx][faceIdx[3*i + 1]];
      int fn2 = elements[eidx][faceIdx[3*i + 2]];

      vector<int> idxs = {fn0, fn1, fn2};
      sort(idxs.begin(), idxs.end());

      string hidx = hashIndices(idxs, maxLen);

      // Check if the face is in faceLookup.
      if (faceLookup.find(hidx) == faceLookup.end()) {
        // If face not in faceLookup, add it and the index associated with it.
        faceLookup.insert({hidx, fIdx});
        
        // Add the face to `faceIndices`.
        faceIndices.push_back(make_tuple(fn0, fn1, fn2));
        
        fIdx++;
      }
    }
  }

  faces.resize(extents[faceIndices.size()][3]);
  for (int i = 0; i < faceIndices.size(); ++i) {
    faces[i][0] = get<0>(faceIndices[i]);
    faces[i][1] = get<1>(faceIndices[i]);
    faces[i][2] = get<2>(faceIndices[i]);
  }
  
}

void Mesh::extractEdges(multi_array<double, 2> const &vertices,
                        multi_array<int,    2> const &elements,
                        multi_array<int,    2>       &edges)
{
  int n0, n1, n2, n3;
  vector<int> edgeIdx = {0, 1,   0, 2,   0, 3,   1, 2,   1, 3,   2, 3};
  unordered_map<string, int> edgeLookup;
  vector< tuple<int , int> > edgeIndices;

  int maxLen = vertices.size();

  for (int eidx = 0; eidx < elements.size(); ++eidx) {
    int eIdx = 0;
    for (int i = 0; i < 6; ++i) {
      // Edge nodes.
      int en0 = elements[eidx][edgeIdx[2*i + 0]];
      int en1 = elements[eidx][edgeIdx[2*i + 1]];

      vector<int> idxs = {en0, en1};
      sort(idxs.begin(), idxs.end());

      string hidx = hashIndices(idxs, maxLen);

      // Check if the edge is in `edgeLookup`.
      if (edgeLookup.find(hidx) == edgeLookup.end()) {
        // If edge not in `edgeLookup`, add it and the index associated with it.
        edgeLookup.insert({hidx, eIdx});
        
        // Add the edge to `edgeIndices`.
        edgeIndices.push_back(make_tuple(en0, en1));
        
        eIdx++;
      }
    }
  }

  edges.resize(extents[edgeIndices.size()][2]);
  for (int i = 0; i < edgeIndices.size(); ++i) {
    edges[i][0] = get<0>(edgeIndices[i]);
    edges[i][1] = get<1>(edgeIndices[i]);
  }
  
}

void Mesh::elemToElem(multi_array<double, 2> const &vertices,
                      multi_array<int,    2> const &elements,
                      vector< vector<int> >        &vtrxConnect,
                      vector< vector<int> >        &edgeConnect,
                      vector< vector<int> >        &faceConnect)
{
  // Permutation of faces per element.
  vector<int> lclFaceIndices = {0, 1, 2,   0, 1, 3,   0, 2, 3,  1, 2, 3};

  // Permutation of edges per element.
  vector<int> lclEdgeIndices = {0, 1,   0, 2,   0, 3,   1, 2,   1, 3,   2, 3};
  
  // maxLen is used to determine hash values.
  int maxLen = vertices.size();

  int nMaxVtrxNeighbour = 0;
  int nMaxEdgeNeighbour = 0;
  int nMaxFaceNeighbour = 0;

  // Lookup tables associate a vector of elements with a unique hash based on 
  // either the vertex, edge or face in a mesh.
  unordered_map< string, vector<int> > vtrxLookup;
  unordered_map< string, vector<int> > edgeLookup;
  unordered_map< string, vector<int> > faceLookup;

  unordered_map< string, vector<int> > vtrxLookupIndices;
  unordered_map< string, vector<int> > edgeLookupIndices;
  unordered_map< string, vector<int> > faceLookupIndices;

  int nVertices = 4;
  int nFaces    = 4;
  int nEdges    = 6;

  for (int elemIndex = 0; elemIndex < elements.size(); ++elemIndex) {
    int vtrxIndex = 0;
    int edgeIndex = 0;
    int faceIndex = 0;

    // Process vertices.
    for (int i = 0; i < nVertices; ++i) {
      int vn0 = elements[elemIndex][i];

      vector<int> idxs = {vn0};

      string hidx = hashIndices(idxs, maxLen);

      // Check if the vertex is in vtrxLookup.
      if (vtrxLookup.find(hidx) == vtrxLookup.end()) {
        // If the vertex is not in `vtrxLookup`, then create a new entry and
        // add the element index associated with it.
        vector<int> vec = {elemIndex};
        vtrxLookup.insert({hidx, vec});
        vtrxLookupIndices.insert({hidx, idxs});
      } else {
        // If the vertex *is* in `vtrxLookup`, then add the element index.
        vtrxLookup[hidx].push_back(elemIndex);
      }

      // Update `nMaxVtrxNeighbour`.
      if (nMaxVtrxNeighbour < vtrxLookup[hidx].size()) {
        nMaxVtrxNeighbour = vtrxLookup[hidx].size();
      }
    }

    // Process faces.
    for (int i = 0; i < nFaces; ++i) {
      int fn0 = elements[elemIndex][lclFaceIndices[3*i + 0]];
      int fn1 = elements[elemIndex][lclFaceIndices[3*i + 1]];
      int fn2 = elements[elemIndex][lclFaceIndices[3*i + 2]];

      vector<int> idxs = {fn0, fn1, fn2};
      sort(idxs.begin(), idxs.end()); // sort for canonical order.

      string hidx = hashIndices(idxs, maxLen);

      // Check if the face is in `faceLookup`.
      if (faceLookup.find(hidx) == faceLookup.end()) {
        // If face not in `faceLookup`, then create a new entry and add the
        // element index associated with it.
        vector<int> vec = {elemIndex};
        faceLookup.insert({hidx, vec});
        faceLookupIndices.insert({hidx, idxs});
      } else  {
        // If the vertex *is* in `faceLookup`, then add the element index.
        faceLookup[hidx].push_back(elemIndex);
      }

      // Update `nMaxFaceNeighbour`.
      if (nMaxFaceNeighbour < faceLookup[hidx].size()) {
        nMaxFaceNeighbour = faceLookup[hidx].size();
      }
    }

    // Process edges.
    for (int i = 0; i < nEdges; ++i) {
      int en0 = elements[elemIndex][lclEdgeIndices[2*i + 0]];
      int en1 = elements[elemIndex][lclEdgeIndices[2*i + 1]];

      vector<int> idxs = {en0, en1};
      sort(idxs.begin(), idxs.end()); // Sort for canonical order.

      string hidx = hashIndices(idxs, maxLen);

      // Check if the edge is in `edgeLookup`.
      if (edgeLookup.find(hidx) == edgeLookup.end()) {
        // If edge not in `edgeLookup`, then create a new entry and add the 
        // element index associated with it.
        vector<int> vec = {elemIndex};
        edgeLookup.insert({hidx, vec});
        edgeLookupIndices.insert({hidx, idxs});
      } else {
        // If the edge *is* in `edgeLookup`, then add the element index.
        edgeLookup[hidx].push_back(elemIndex);
      }

      // Update `nMaxEdgeNeighbour`.
      if (nMaxEdgeNeighbour < edgeLookup[hidx].size()) {
        nMaxEdgeNeighbour = edgeLookup[hidx].size();
      }
    }
  }

  // Fill in element connectivity by vertex.
  vtrxConnect.resize(elements.size());
  for (auto itr = vtrxLookup.begin(); itr != vtrxLookup.end(); ++itr) {
    vector<int> &indices = itr->second;
    for (int i = 0; i < indices.size(); ++i) {
      int eidx = indices[i];
      for (int j = 0; j < indices.size(); ++j) {
        if (i != j) {
          vtrxConnect[eidx].push_back(indices[j]);
        }
      }
    }
  }

  // Fill in element connectivity by edge.
  edgeConnect.resize(elements.size());
  for (auto itr = edgeLookup.begin(); itr != edgeLookup.end(); ++itr) {
    vector<int> &indices = itr->second;
    for (int i = 0; i < indices.size(); ++i) {
      int eidx = indices[i];
      for (int j = 0; j < indices.size(); ++j) {
        if (i != j) {
          edgeConnect[eidx].push_back(indices[j]);
        }
      }
    }
  }

  // Fill in element connectivity by face.
  faceConnect.resize(elements.size());
  for (auto itr = faceLookup.begin(); itr != faceLookup.end(); ++itr) {
    vector<int> &indices = itr->second;
    for (int i = 0; i < indices.size(); ++i) {
      int eidx = indices[i];
      for (int j = 0; j < indices.size(); ++j) {
        if (i != j) {
          faceConnect[eidx].push_back(indices[j]);
        }
      }
    }
  }

  // Calculate boundary faces.
  

}

string Mesh::hashIndices(vector<int> &indices, int maxLen)
{
  // Sort the indices so that we always have a canonical order.
  sort(indices.begin(), indices.end());

  string hash;
  for (auto index : indices) {
    // Create a string version of the current `index`.
    string strIndex;
    ostringstream ssIndex;
    ssIndex << index;

    if (maxLen < strIndex.length()) {
      // If the `strIndex` is larger than the `maxLen` then just use it as is.
      hash += strIndex;
    } else {
      // Otherwise, pad the string up front so that it is of length `maxLen`.
      int niter = maxLen - strIndex.length();
      for (int i = 0; i < niter; ++i) {
        strIndex = "x" + strIndex;
      }
      hash += strIndex;
    }
  }

  return hash;
}
