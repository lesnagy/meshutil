/*
 * @file   tetra_mesh_utils.cxx
 * @author L. Nagy
 */

#include "tetra_mesh_utils.h"

using namespace std;
using namespace boost;

void vertex_to_element_mapping(
    multi_array<double, 2> const &vertices,
    multi_array<int,    2> const &elements,
    multi_array<int,    2>       &mapping)
{
  size_t nvert = vertices.shape()[0];

  unordered_map< int, vector<int> > map;

  int width = 0;
  for (int i = 0; i < elements.shape()[0]; ++i ) {
    for (int j = 0; j < elements.shape()[1]; ++j) {
      int n = elements[i][j];
      if (map.find(n) == map.end()) {
        vector<int> newRow;
        map.insert({n, newRow});
      }
      map[n].push_back(i);
      if (width < map[n].size()) {
        width = map[n].size();
      }
    }
  } 

  // Resize and fill the output `mapping`.
  mapping.resize(extents[nvert][width]);
  fill(&mapping[0][0], 
       &mapping[0][0]+(mapping.shape()[0]*mapping.shape()[1]), 
       -1);

  // Construct vertex to elements mapping.
  for (int i = 0; i < mapping.shape()[0]; ++i) {
    sort(map[i].begin(), map[i].end());
    for (int j = 0; j < map[i].size(); ++j) {
      mapping[i][j] = map[i][j];
    }
  }
}

void vertex_to_face_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &faces,
    boost::multi_array<int,    2>       &mapping)
{
  int nvert = vertices.shape()[0];
  unordered_map< int, vector<int> > map(nvert);

  int width = 0;
  for (int i = 0; i < faces.shape()[0]; ++i) {
    for (int j = 0; j < faces.shape()[1]; ++j) {
      int n = faces[i][j];
      if (map.find(n) == map.end()) {
        vector<int> newRow;
        map.insert({n, newRow});
      }
      map[n].push_back(i);
      if (width < map[n].size()) {
        width = map[n].size();
      }
    }
  }

  // Resize and fill the output `mapping`.
  mapping.resize(extents[nvert][width]);
  fill(&mapping[0][0],
       &mapping[0][0]+(mapping.shape()[0]*mapping.shape()[1]),
       -1);

  // Construct vertex to face mapping.
  for (int i = 0; i < mapping.shape()[0]; ++i) {
    sort(map[i].begin(), map[i].end());
    for (int j = 0; j < map[i].size(); ++j) {
      mapping[i][j] = map[i][j];
    }
  }

}

void extract_faces(
    multi_array<double, 2> const &vertices,
    multi_array<int,    2> const &elements,
    multi_array<int,    2>       &faces)
{
  int n0, n1, n2, n3;
  vector<int> faceIdx = {0, 1, 2,   0, 1, 3,   0, 2, 3,  1, 2, 3};
  unordered_map<string, int> faceLookup;
  vector< tuple<int , int, int> > faceIndices;

  int maxlen = vertices.size();

  for (int eidx = 0; eidx < elements.size(); ++eidx) {
    // Element nodes.
    int en0 = elements[eidx][0];
    int en1 = elements[eidx][1];
    int en2 = elements[eidx][2];
    int en3 = elements[eidx][3];

    int fIdx = 0;
    for (int i = 0; i < 4; ++i) {
      // Face nodes.
      int fn0 = elements[eidx][faceIdx[3*i + 0]];
      int fn1 = elements[eidx][faceIdx[3*i + 1]];
      int fn2 = elements[eidx][faceIdx[3*i + 2]];

      vector<int> idxs = {fn0, fn1, fn2};
      sort(idxs.begin(), idxs.end());

      string hidx = hash_indices(idxs, maxlen);

      // Check if the face is in faceLookup.
      if (faceLookup.find(hidx) == faceLookup.end()) {
        // If face not in faceLookup, add it and the index associated with it.
        faceLookup.insert({hidx, fIdx});
        
        // Add the face to `faceIndices`.
        faceIndices.push_back(make_tuple(fn0, fn1, fn2));
        
        fIdx++;
      }
    }
  }

  faces.resize(extents[faceIndices.size()][3]);
  for (int i = 0; i < faceIndices.size(); ++i) {
    faces[i][0] = get<0>(faceIndices[i]);
    faces[i][1] = get<1>(faceIndices[i]);
    faces[i][2] = get<2>(faceIndices[i]);
  }
  
}

void face_to_element_mapping(multi_array<double, 2> const &vertices,
                             multi_array<int,    2> const &elements,
                             multi_array<int,    2> const &faces,
                             multi_array<int,    2>       &mapping)
{
  vector< vector<int> > map;

  vector<int> faceIdx = {0, 1, 2,   0, 1, 3,   0, 2, 3,  1, 2, 3};
  unordered_map<string, int> faceLookup;
  vector< tuple<int, int , int, int> > faceIndices;

  int maxlen = vertices.size();

  int mapWidth = 0;
  int fIdx = 0;
  for (int eidx = 0; eidx < elements.size(); ++eidx) {
    // Element nodes.
    int en0 = elements[eidx][0];
    int en1 = elements[eidx][1];
    int en2 = elements[eidx][2];
    int en3 = elements[eidx][3];

    for (int i = 0; i < 4; ++i) {
      // Face nodes.
      int fn0 = elements[eidx][faceIdx[3*i + 0]];
      int fn1 = elements[eidx][faceIdx[3*i + 1]];
      int fn2 = elements[eidx][faceIdx[3*i + 2]];

      vector<int> idxs = {fn0, fn1, fn2};
      sort(idxs.begin(), idxs.end());

      string hidx = hash_indices(idxs, maxlen);

      // Check if the face is in `faceLookup`.
      if (faceLookup.find(hidx) == faceLookup.end()) {
        // If face not in `faceLookup`, add it and the index associated with it.
        faceLookup.insert({hidx, fIdx});

        // Check that the face index `fIdx` is reffering to is consistent with
        // the `faces` table.
        if ( (faces[fIdx][0] != fn0) 
            || (faces[fIdx][1] != fn1) 
            || (faces[fIdx][2] != fn2) ) {
          cout << "WARNING: face index inconsistency!" << endl;
        }

        // Create a new row in the `map` with index fIdx.
        vector<int> newrow;
        map.push_back(newrow);

        fIdx++;
      }

      // Add the element index for the face.
      map[faceLookup[hidx]].push_back(eidx);

      // Make sure to keep track of the maximum width of the `map` table.
      if (mapWidth < map[faceLookup[hidx]].size()) {
        mapWidth = map[faceLookup[hidx]].size();
      }
    }
  }

  // Final processing here.
  mapping.resize(extents[map.size()][mapWidth]);
  fill(&mapping[0][0], 
       &mapping[0][0] + (mapping.shape()[0]* mapping.shape()[1]),
       -1);

  for (int i = 0; i < map.size(); ++i) {
    sort(map[i].begin(), map[i].end());
    for (int j = 0; j < map[i].size(); ++j) {
      mapping[i][j] = map[i][j];
    }
  }
}

void extract_surface_faces(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &elements,
    boost::multi_array<int,    2> const &f2e,
    boost::multi_array<int,    2>       &surfaceFaces)
{
  // void vector< vector<int> > map;
}

void element_to_element_face_mapping(
    multi_array<double, 2> const &vertices,
    multi_array<int,    2> const &elements,
    multi_array<int,    2>       &mappping)
{
}

void surface_vertex_to_surface_face_mapping(
    boost::multi_array<double, 2> const &vertices,
    boost::multi_array<int,    2> const &faces,
    boost::multi_array<int,    2> const &fe_mapping,
    boost::multi_array<int,    2>       &mapping)
{
  unordered_map< int, vector<int> > map;
  int nvert = vertices.shape()[0];

  int width = 0;
  for (int fidx = 0; fidx < faces.shape()[0]; ++fidx) {
    // Look up face index `fidx` in `fe_mapping` and see if there is a -1 in the 
    // 2nd position (there should never be more than two positions and the first
    // position should always be filled).
    if ((fe_mapping[fidx][0] != -1) && (fe_mapping[fidx][1] == -1)) {
      for (int i = 0; i < faces.shape()[1]; ++i) {
        int n = faces[fidx][i]; // This is the vertex id of a face.
        if (map.find(n) == map.end()) {
          vector<int> newRow;
          map.insert({n, newRow});
        }
        map[n].push_back(fidx);
        if (width < map[n].size()) {
          width = map[n].size();
        }
      }
    }
  }

  // Resize and fill the output `mapping`.
  mapping.resize(extents[nvert][width]);
  fill(&mapping[0][0],
       &mapping[0][0]+(mapping.shape()[0]*mapping.shape()[1]),
       -1);

  // Construct the face mapping.
  for (int i = 0; i < nvert; ++i) {
    // If the vertex id is not in the map, just skip over it.
    if (map.find(i) != map.end()) {
      for (int j = 0; j < map[i].size(); ++j) {
        mapping[i][j] = map[i][j];
      }
    }
  }
}

string hash_indices(vector<int> indices, size_t maxlen) {
  sort(indices.begin(), indices.end());

  string hash;
  for (int i = 0; i < indices.size(); ++i) {
    int idx = indices[i];
    hash += padded_number(idx, maxlen);
  }

  return hash;
}

string padded_number(int number, size_t maxlen) {
  string strNumber; 
  ostringstream ss;

  ss << number;
  strNumber = ss.str();

  if (maxlen < strNumber.length()) {
    return strNumber;
  }

  int niter = maxlen - strNumber.length();
  for (int i = 0; i < niter; ++i) {
    strNumber = "x" + strNumber;
  }

  return strNumber;
}

void calculate_face_info(boost::multi_array<double, 2> const &vertices,
                         int n0, int n1, int n2, int n3,
                         int f0, int f1, int f2,
                         int &winding,
                         double &fx, double &fy, double &fz,
                         double &nx, double &ny, double &nz)
{
  double norm;

  // The extract element and face vertex coordinates - this involves some
  // repetition (since faces belong to elements) but saves on bookkeeping.
  double n0x = vertices[n0][0], n0y = vertices[n0][1], n0z = vertices[n0][2];
  double n1x = vertices[n1][0], n1y = vertices[n1][1], n1z = vertices[n1][2];
  double n2x = vertices[n2][0], n2y = vertices[n2][1], n2z = vertices[n2][2];
  double n3x = vertices[n3][0], n3y = vertices[n3][1], n3z = vertices[n3][2];

  // The element centroid is the average of the element coordinates.
  double cx = (n0x + n1x + n2x + n3x)/4.0;
  double cy = (n0y + n1y + n2y + n3y)/4.0;
  double cz = (n0z + n1z + n2z + n3z)/4.0;

  double f0x = vertices[f0][0], f0y = vertices[f0][1], f0z = vertices[f0][2];
  double f1x = vertices[f1][0], f1y = vertices[f1][1], f1z = vertices[f1][2];
  double f2x = vertices[f2][0], f2y = vertices[f2][1], f2z = vertices[f2][2];

  // The winding is related to the order of f0, f1, f2. Calculate u and v 
  // vectors with respect to this order.
  double ux = f1x - f0x, uy = f1y - f0y, uz = f1z - f0z;
  double vx = f2x - f1x, vy = f2y - f1y, vz = f2z - f1z;

  // The normal is the cross product of u & v.
  nx = uy*vz - uz*vy;
  ny = uz*vx - ux*vz;
  nz = ux*vy - uy*vx;
  
  norm = sqrt(nx*nx + ny*ny + nz*nz);
  nx = nx/norm; ny = ny/norm; nz = nz/norm;

  // The face centroid is the average of the face coordinates.
  fx = (f0x + f1x + f2x)/3.0;
  fy = (f0y + f1y + f2y)/3.0;
  fz = (f0z + f1z + f2z)/3.0;

  // Construct a vector from the element centroid to the face centroid.
  double wx = fx - cx,   wy = fy - cy,   wz = fz - cz;
  
  norm = sqrt(wx*wx + wy*wy + wz*wz);
  wx = wx/norm; wy = wy/norm; wz = wz/norm;

  // Calculate the dot product of normal and w vector.
  double dot = nx*wx + ny*wy + nz*wz;

  if (dot > 0) {
    winding = RIGHT_HANDED_WINDING;
  } else {
    winding = LEFT_HANDED_WINDING;
    // Fix the normal vector so that it points out.
    nx *= -1.0; ny *= -1.0; nz *= -1.0;
  }
}

void calculate_element_info(boost::multi_array<double, 2> const &vertices,
                            int n0, int n1, int n2, int n3,
                            double &cx, double &cy, double &cz,
                            double &vol, double &volsign)
{
  // The extract element and face vertex coordinates.
  double n0x = vertices[n0][0], n0y = vertices[n0][1], n0z = vertices[n0][2];
  double n1x = vertices[n1][0], n1y = vertices[n1][1], n1z = vertices[n1][2];
  double n2x = vertices[n2][0], n2y = vertices[n2][1], n2z = vertices[n2][2];
  double n3x = vertices[n3][0], n3y = vertices[n3][1], n3z = vertices[n3][2];

  // The element centroid is the average of the element coordinates.
  cx = (n0x + n1x + n2x + n3x)/4.0;
  cy = (n0y + n1y + n2y + n3y)/4.0;
  cz = (n0z + n1z + n2z + n3z)/4.0;

  // The element volume.
  vol = - n0z*n1y*n2x + n0y*n1z*n2x + n0z*n1x*n2y - n0x*n1z*n2y 
        - n0y*n1x*n2z + n0x*n1y*n2z + n0z*n1y*n3x - n0y*n1z*n3x 
        - n0z*n2y*n3x + n1z*n2y*n3x + n0y*n2z*n3x - n1y*n2z*n3x 
        - n0z*n1x*n3y + n0x*n1z*n3y + n0z*n2x*n3y - n1z*n2x*n3y 
        - n0x*n2z*n3y + n1x*n2z*n3y + n0y*n1x*n3z - n0x*n1y*n3z 
        - n0y*n2x*n3z + n1y*n2x*n3z + n0x*n2y*n3z - n1x*n2y*n3z;

  // The sign of the volume.
  volsign = copysign(1.0, vol);

  // Take the absolute value of the volume.
  vol = fabs(vol);
}
