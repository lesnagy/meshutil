cmake_minimum_required(VERSION 2.6)

project(meshutil)
#enable_language(Fortran)
enable_testing()

find_package(Boost REQUIRED COMPONENTS regex)
if(Boost_FOUND)
  message("Boost_LIBRARIES: ${Boost_LIBRARIES}")
  include_directories(${Boost_INCLUDE_DIRS})
endif()

find_package(meshfileio QUIET CONFIG)
if (${meshfileio_FOUND})
  include_directories(${meshfileio_include_dir})
endif()

set(SRC_DIR src)
set(INCLUDE_DIR src/include)
set(TEST_SRC_DIR ${SRC_DIR}/test)

include_directories(${INCLUDE_DIR})

add_library(meshutil STATIC ${SRC_DIR}/tetra_mesh_utils.cxx
                            ${SRC_DIR}/Mesh.cpp)

install(TARGETS meshutil
        ARCHIVE DESTINATION lib)
install(DIRECTORY ${INCLUDE_DIR}
        DESTINATION .
        FILES_MATCHING PATTERN "*.h")
install(FILES "meshutil-config.cmake"
        DESTINATION .)

###############################################################################
# Test executables.                                                           #
###############################################################################
add_custom_target(build-tests)
add_dependencies(build-tests
  cpp_vertex_to_element_test
  cpp_padded_number_test
  cpp_hash_indices_test
  cpp_element_to_element_face_mapping_test
  cpp_face_to_element_mapping_test
  cpp_extract_faces_test
  cpp_calculate_face_info_test
  cpp_calculate_element_info_test
  cpp_vertex_to_face_mapping_test
  cpp_surface_vertex_to_surface_face_mapping_test)


#-- Test for vertex to element mapping --#
add_executable(cpp_vertex_to_element_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_vertex_to_element.cxx)
target_link_libraries(cpp_vertex_to_element_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_vertex_to_element_test 
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_vertex_to_element_test)

#-- Test for routine that will create a padded string for an integer --#
add_executable(cpp_padded_number_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_padded_number.cxx)
target_link_libraries(cpp_padded_number_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_padded_number_test 
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_padded_number_test)

#-- Test for routine that will create a hash from n integers --#
add_executable(cpp_hash_indices_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_hash_indices_test.cxx)
target_link_libraries(cpp_hash_indices_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_hash_indices_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_hash_indices_test)

# Test for map of element-element connections (by element face) --#
add_executable(cpp_element_to_element_face_mapping_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_element_to_element_face_mapping_test.cxx)
target_link_libraries(cpp_element_to_element_face_mapping_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_element_to_element_face_mapping_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_element_to_element_face_mapping_test)

#-- Test for face-element connections --#
add_executable(cpp_face_to_element_mapping_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_face_to_element_mapping_test.cxx)
target_link_libraries(cpp_face_to_element_mapping_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_face_to_element_mapping_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_face_to_element_mapping_test)

#-- Test for routine to extract faces --#
add_executable(cpp_extract_faces_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_extract_faces_test.cxx)
target_link_libraries(cpp_extract_faces_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_extract_faces_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_extract_faces_test)

#-- Test for routine to calculate some useful face information --#
add_executable(cpp_calculate_face_info_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_calculate_face_info_test.cxx)
target_link_libraries(cpp_calculate_face_info_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_calculate_face_info_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_calculate_face_info_test)

#-- Test for routine to calculate some useful element information --#
add_executable(cpp_calculate_element_info_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_calculate_element_info_test.cxx)
target_link_libraries(cpp_calculate_element_info_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_calculate_element_info_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_calculate_element_info_test)

#-- Test for vertex-face connections --#
add_executable(cpp_vertex_to_face_mapping_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_vertex_to_face_mapping_test.cxx)
target_link_libraries(cpp_vertex_to_face_mapping_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_vertex_to_face_mapping_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_vertex_to_face_mapping_test)

#-- Test for surface vertex - surface face connections --#
add_executable(cpp_surface_vertex_to_surface_face_mapping_test EXCLUDE_FROM_ALL
  ${TEST_SRC_DIR}/cpp_surface_vertex_to_surface_face_mapping_test.cxx)
target_link_libraries(cpp_surface_vertex_to_surface_face_mapping_test 
  meshutil
  ${meshfileio_libraries}
  ${Boost_LIBRARIES})
add_test(NAME CPP_surface_vertex_to_surface_face_mapping_test
  WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
  COMMAND cpp_surface_vertex_to_surface_face_mapping_test)







       

