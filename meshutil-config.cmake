#
# Library configuration file used by dependent projects.
# @file:   meshutil-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED meshutil_FOUND)

  # Locate the library headers.
  find_path(meshutil_include_dir
    NAMES tetra_mesh_utils.h
    PATHS ${meshutil_DIR}/include
  )

  # Locate libraries.
  find_library(meshutil_libraries
    meshutil
    PATHS ${meshutil_DIR}/lib
  )

  # Export libary targets.
#  set(meshutil_libraries
#    meshutil
#    CACHE INTERNAL "meshutil library" FORCE
#  )

  # Handle REQUIRED, QUIET and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    meshutil DEFAULT_MSG
    meshutil_include_dir
    meshutil_libraries
  )

#  # If the project is not `meshutil` then make directories. 
#  if (NOT ${PROJECT_NAME} STREQUAL meshutil)
#    add_subdirectory(
#      ${meshutil_DIR}
#      ${CMAKE_CURRENT_BINARY_DIR}/meshutil
#    )
#  endif()

endif()
